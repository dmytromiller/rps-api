// rulesSet {'rule 1':'rule1func', 'rule 2':'rule2func'}

exports.checkParameters = function (paramObj, rulesSet = null, allowedParameters = null) {
    let result = null;

    if(allowedParameters) { 
        // parameters check required
        const objKeys = Object.keys(paramObj);

        for(let i = 0; i < objKeys.length; i++) {
            let param = objKeys[i];

            if(!allowedParameters.includes(param)) {
                if(result) {
                    result += '\n';
                } else {
                    result = '';
                }
                result += "Parameter '" + param + "' is not allowed";
            }
        }
    }
    
    if(rulesSet) {
        for (const rule in rulesSet) {
            let checkFunc = eval(rulesSet[rule]);
            let checkRes = checkFunc(paramObj);

            if(!checkRes) {
                if(result) {
                    result += '\n';
                } else {
                    result = '';
                }
                result += rule + ": false";
            }
        }
    }
    return result;
}