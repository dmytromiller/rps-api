'use strict';
var mysql = require('mysql');

const deleteIn = 3; //minutes
const deleteLong = 180; //minutes

const headText = {'Access-Control-Allow-Origin': '*', 'Content-Type': 'text/plain'};
const headJson = {'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json'};

var mySQLConnection = mysql.createConnection({
    host: "localhost",
    user: "root", //"orientee_rps_admin",
    password: "Dima0372", //"Rps-Ad!123",
    database: "orientee_dm_rps"
});

mySQLConnection.connect(function(errCon) {
    if (errCon) {
        console.log('Error connecting to the DB, '); 
        console.log(errCon);
    }
});


exports.roomRegister = (roomID, response) => {
    const roomDB = roomID.toLowerCase().replace(/"/, '""'); 

    var con = mySQLConnection;

    var sql = "DELETE FROM room WHERE ((RoomID = \""+ roomDB + "\") AND (LastOperation < DATE_SUB(Now(), INTERVAL " + deleteIn + " MINUTE))) OR ((LastOperation < DATE_SUB(Now(), INTERVAL " + deleteLong + " MINUTE)))";
    con.query(sql, function (errDel, resultDelete) {
        if (errDel) {
            console.log('Error executing DELETE statement, roomRegister, SQL:' + sql); 
            console.log(errDel);
            internalError(response, "roomRegister -> Delete");
        } else {
            var sql = "SELECT * FROM room WHERE (RoomID = \""+ roomDB + "\")";
            con.query(sql, function (errSel, resultSelect, fields) {
                if (errSel) {
                    console.log('Error executing SELECT statement, roomRegister, SQL:' + sql); 
                    console.log(errSel);
                    internalError(response, "roomRegister -> Select");
                } else {        
                    if(resultSelect.length === 0) {
                        var sql = "INSERT INTO room (RoomID) VALUES (\"" + roomDB + "\");";
                        con.query(sql, function (errIns, resultInsert) {
                            if (errIns) {
                                console.log('Error executing INSERT statement, roomRegister, SQL:' + sql); 
                                console.log(errIns);
                                internalError(response, "roomRegister -> Insert");
                            } else {
                                updateUser([{User1:0, User2:0}], response, con, roomID, roomDB);
                            }
                        });
                    } else {
                        updateUser(resultSelect, response, con, roomID, roomDB);
                    }
                }
            });
        }
    });        
}

exports.roomStatus = (roomID, userID, response) => {
    const roomDB = roomID.toLowerCase().replace(/"/, '""'); 

    var con = mySQLConnection;
    
    var sql = "SELECT * FROM room WHERE (RoomID = \""+ roomDB + "\") AND ((User1 = " + userID +") OR (User2 = " + userID +"))";

    con.query(sql, function (errSel, resultSelect, fields) {
        if (errSel) {
            console.log('Error executing SELECT statement, roomStatus, SQL:' + sql); 
            console.log(errSel);
            internalError(response, "roomStatus -> Select");
        } else if (resultSelect.length === 0) {
            response.writeHead(404, headText);
            response.end('Record not found');
        } else {
            response.writeHead(200, headJson);
            const roomRecord = resultSelect[0];

            if (roomRecord.User2 == 0) {
                var respJson = {Status:1, StatusText:'Waiting for your buddy to join'};
            } else {
                const userView = (userID == roomRecord.User1) ? 1 : 2;

                if(roomRecord['Status' + userView] == 0) {
                    var respJson = {Status:2, StatusText:'Waiting for your turn'};
                } else if(roomRecord['Status' + ((userView == 1) ? 2 : 1)] == 0) {
                    var respJson = {Status:3, StatusText:"Waiting for your buddy's turn"};
                } else {
                    var win1 = Number(roomRecord.Win1);
                    var win2 = Number(roomRecord.Win2);

                    var stat1 = Number(roomRecord.Status1);
                    var stat2 = Number(roomRecord.Status2);

                    if(stat1 == stat2) {
                        var winner = 0;
                    } else if(Math.abs(stat1-stat2) == 1) {
                        var winner = (stat1 > stat2) ? 1 : 2;
                    } else {
                        var winner = (stat1 < stat2) ? 1 : 2;
                    }

                    var winStatus = (winner == 0) ? 0 : (winner == userView) ? 1 : -1

                    var respJson = {Status:4, StatusText:'Game Over', WinStatus: winStatus, YourTurn: (userView == 1) ? stat1 : stat2, BuddyTurn: (userView == 2) ? stat1 : stat2, YourWin: (userView == 1) ? win1 : win2, BuddyWin: (userView == 2) ? win1 : win2};
                }
            }                    
            response.end(JSON.stringify(respJson));
        }
    });        
}

exports.userMove = (roomID, userID, userTurn, response) => {
    const roomDB = roomID.toLowerCase().replace(/"/, '""'); 

    var con = mySQLConnection;
    
    var sql = "SELECT * FROM room WHERE (RoomID = \""+ roomDB + "\") AND ((User1 = " + userID +") OR (User2 = " + userID +"))";

    con.query(sql, function (errSel, resultSelect, fields) {
        if (errSel) {
            console.log('Error executing SELECT statement, userMove, SQL:' + sql); 
            console.log(errSel);
            internalError(response, "userMove -> Select");
        } else if (resultSelect.length === 0) {
            response.writeHead(404, headText);
            response.end('Record not found');
        } else {
            var roomRecord = resultSelect[0];
            var userView = (userID == roomRecord.User1) ? 1 : 2;
            var userBuddy = 3 - userView;

            if((roomRecord.Status1 == 0) || (roomRecord.Status2 == 0)) {
                var sql = "UPDATE room SET Status" + userView + " = " + userTurn;

                if(roomRecord['Status' + userBuddy] != 0) {
                    roomRecord['Status'+ userView] = userTurn;
                    var stat1 = Number(roomRecord.Status1);
                    var stat2 = Number(roomRecord.Status2);

                    if(stat1 == stat2) {
                        var winner = 0;
                    } else if(Math.abs(stat1-stat2) == 1) {
                        var winner = (stat1 > stat2) ? 1 : 2;
                    } else {
                        var winner = (stat1 < stat2) ? 1 : 2;
                    }

                    var winStatus = (winner == 0) ? 0 : ((winner == userView) ? 1 : -1);
                    
                    if(winStatus == 1) {
                        roomRecord['Win'+ userView] = Number(roomRecord['Win'+ userView])+1; 

                        sql += ", Win" + userView + " = Win" + userView + " + 1"
                    } else if(winStatus == -1) {
                        roomRecord['Win'+ userBuddy] = Number(roomRecord['Win'+ userBuddy])+1; 

                        sql += ", Win" + userBuddy + " = Win" + userBuddy + " + 1"
                    } 

                    var win1 = Number(roomRecord.Win1);
                    var win2 = Number(roomRecord.Win2);
                } 

                sql += " WHERE (RoomID = \"" + roomDB + "\");";
                con.query(sql, function (errUpd, resUpd) {
                    if (errUpd) {
                        console.log('Error executing UPDATE statement, userMove, SQL:' + sql); 
                        console.log(errUpd);
                        internalError(response, "userMove -> Update");                              
                    } else {
                        roomRecord['Status' + userView] = userTurn;

                        response.writeHead(200, headJson);

                        if (roomRecord.User2 == 0) {
                            var respJson = {Status:1, StatusText:'Waiting for your buddy to join'};
                        } else if(roomRecord['Status' + userBuddy] == 0) {
                            var respJson = {Status:3, StatusText:"Waiting for your buddy's turn"};
                        } else {
                            var respJson = {Status:4, StatusText:'Game Over', WinStatus: winStatus, YourTurn: (userView == 1) ? stat1 : stat2, BuddyTurn: (userView == 2) ? stat1 : stat2, YourWin: (userView == 1) ? win1 : win2, BuddyWin: (userView == 2) ? win1 : win2};
                        }
                        response.end(JSON.stringify(respJson));                    
                    }
                    
                });
            } else {
                var win1 = Number(roomRecord.Win1);
                var win2 = Number(roomRecord.Win2);

                var stat1 = Number(roomRecord.Status1);
                var stat2 = Number(roomRecord.Status2);

                if(stat1 == stat2) {
                    var winner = 0;
                } else if(Math.abs(stat1-stat2) == 1) {
                    var winner = (stat1 > stat2) ? 1 : 2;
                } else {
                    var winner = (stat1 < stat2) ? 1 : 2;
                }

                var winStatus = (winner == 0) ? 0 : (winner == userView) ? 1 : -1

                response.writeHead(200, headJson);
                var respJson = {Status:4, StatusText:'Game Over', WinStatus: winStatus, YourTurn: (userView == 1) ? stat1 : stat2, BuddyTurn: (userView == 2) ? stat1 : stat2, YourWin: (userView == 1) ? win1 : win2, BuddyWin: (userView == 2) ? win1 : win2};
                response.end(JSON.stringify(respJson));
            }
        }
    });        
}

exports.newGame = (roomID, userID, response) => {
    const roomDB = roomID.toLowerCase().replace(/"/, '""'); 

    var con = mySQLConnection;
    
    var sql = "SELECT * FROM room WHERE (RoomID = \""+ roomDB + "\") AND ((User1 = " + userID +") OR (User2 = " + userID +"))";

    con.query(sql, function (errSel, resultSelect, fields) {
        if (errSel) {
            console.log('Error executing SELECT statement, newGame, SQL:' + sql); 
            console.log(errSel);
            internalError(response, "newGame -> Select");
        } else if (resultSelect.length === 0) {
            response.writeHead(404, headText);
            response.end('Record not found');
        } else {
            var roomRecord = resultSelect[0];

            if((roomRecord.Status1 != 0) && (roomRecord.Status2 != 0)) {
                var sql = "UPDATE room SET Status1 = 0, Status2 = 0"; 
                sql += " WHERE (RoomID = \"" + roomDB + "\");";
                con.query(sql, function (errUpd, resUpd) {
                    if (errUpd) {
                        console.log('Error executing UPDATE statement, newGame, SQL:' + sql); 
                        console.log(errUpd);
                        internalError(response, "newGame -> Update");                              
                    } else {
                        response.writeHead(200, headText);
                        response.end('Ok');
                    }
                });
            } else {
                response.writeHead(200, headText);
                response.end('Ok');
            }
        }
    });
}

function updateUser(records, response, con, roomID, roomDB) {
    var userUpdate = 1;
    var userValue = 0;

    if(records[0].User1 != 0) {
        if(records[0].User2 == 0) {
            userUpdate = 2;
            userValue = records[0].User1;
        } else {
            userUpdate = 3;
        }
    }

    if(userUpdate <= 2) {
        var userID = 0;
    
        do {
            userID = Math.floor(Math.random()* 4294967295) + 1;
        }
        while(userID === userValue);

        var sql = "UPDATE room SET User" + userUpdate + " = " + userID + " WHERE (RoomID = \"" + roomDB + "\");";
        con.query(sql, function (errUpd, resUpd) {
            if (errUpd) {
                console.log('Error executing UPDATE statement, roomRegister, SQL:' + sql); 
                console.log(errUpd);
                internalError(response, "roomRegister -> Update");                              
            } else {
                response.writeHead(200, headText);
                response.end(userID.toString());
            }
        });
    } else {
        response.writeHead(403, headText);
        response.end('The room is already occupied and currently in use: ' + roomID);
    }
}

function internalError(response, reason) {
    response.writeHead(503, headText);
    response.end('Error while communicating with DB engine, details: ' + reason);
}