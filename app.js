'use strict';

var http = require('http');
var url = require('url');

var apiCalls = require('./apicalls');
var pch = require('./paramcheck');

var appPath = '/rps/api/';

const headText = {'Access-Control-Allow-Origin': '*', 'Content-Type': 'text/plain'};

var proc = function (req, res) {
    const urlparse = url.parse(req.url, true);
    const urlpath = urlparse.pathname;
    const queryObject = urlparse.query;
    const commands = urlpath.split(appPath);
    let ruleSet, params, chechResult;

    if(commands.length !== 2) {
        res.writeHead(418, headText);
        res.end('No idea what to do with your request');
    } else {
        switch (commands[1]) {
            case 'reg':
                ruleSet = {"Quesry parameter 'room' is missing":"(obj)=>(obj.room>'')"}
                params = ['room'];

                if(!(chechResult = pch.checkParameters(queryObject, ruleSet, params))) {
                    apiCalls.roomRegister(queryObject.room, res);
                } else {
                    chechResult = `Wrong call of reg API\n${chechResult}`;
                    res.writeHead(400, headText);
                    res.end(chechResult);      
                }
                break;

            case 'stat':
                ruleSet = {"Parameter 'room' is Text":"(obj)=>(obj.room>'')", 
                    "Parameter 'user' is Integer":"(obj)=>(Number.isInteger(Number(obj.user)))",
                    "Parameter 'user' > 0":"(obj)=>(Number(obj.user)>0)"
                }
                params = ['room', 'user'];

                if(!(chechResult = pch.checkParameters(queryObject, ruleSet, params))) {
                    apiCalls.roomStatus(queryObject.room, Number(queryObject.user), res);
                } else {
                    chechResult = `Wrong call of stat API\n${chechResult}`;
                    res.writeHead(400, headText);
                    res.end(chechResult);     
                }
                break;

            case 'turn':
                ruleSet = {"Parameter 'room' is Text":"(obj)=>(obj.room>'')", 
                    "Parameter 'user' is Integer":"(obj)=>(Number.isInteger(Number(obj.user)))",
                    "Parameter 'user' > 0":"(obj)=>(Number(obj.user)>0)",
                    "Parameter 'turn' is Integer":"(obj)=>(Number.isInteger(Number(obj.turn)))",
                    "Parameter 'turn' in range 1..3":"(obj)=>{userTurn = Number(obj.turn); return((userTurn > 0) && (userTurn < 4));}"
                }
                params = ['room', 'user', 'turn'];

                if(!(chechResult = pch.checkParameters(queryObject, ruleSet, params))) {
                    apiCalls.userMove(queryObject.room, Number(queryObject.user), Number(queryObject.turn), res);
                } else {
                    chechResult = `Wrong call of turn API\n${chechResult}`;
                    res.writeHead(400, headText);
                    res.end(chechResult);     
                }
                break;

            case 'newgame':
                ruleSet = {"Parameter 'room' is Text":"(obj)=>(obj.room>'')", 
                    "Parameter 'user' is Integer":"(obj)=>(Number.isInteger(Number(obj.user)))",
                    "Parameter 'user' > 0":"(obj)=>(Number(obj.user)>0)"
                }
                params = ['room', 'user'];

                if(!(chechResult = pch.checkParameters(queryObject, ruleSet, params))) {
                    apiCalls.newGame(queryObject.room, Number(queryObject.user), res);
                } else {
                    chechResult = `Wrong call of newgame API\n${chechResult}`;
                    res.writeHead(400, headText);
                    res.end(chechResult);     
                }
                break;

            default:
                res.writeHead(400, headText);
                res.end("Undefined command:" + commands[1]);
                break;
        }
    }
}

var server = http.createServer(proc);
server.listen(9000);
